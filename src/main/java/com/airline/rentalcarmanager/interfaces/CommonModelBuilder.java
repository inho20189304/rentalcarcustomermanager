package com.airline.rentalcarmanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
