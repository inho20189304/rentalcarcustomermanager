package com.airline.rentalcarmanager.service;

import com.airline.rentalcarmanager.entity.Customer;
import com.airline.rentalcarmanager.model.CustomerItem;
import com.airline.rentalcarmanager.model.CustomerRequest;
import com.airline.rentalcarmanager.model.ListResult;
import com.airline.rentalcarmanager.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public void setCustomer(CustomerRequest request) {
        Customer addData = new Customer.CustomerBuilder(request).build();

        customerRepository.save(addData);
    }

    public ListResult<CustomerItem> getCustomers() {
        List<Customer> originList = customerRepository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new CustomerItem.CustomerItemBuilder(item).build()));
        return ListConvertService.settingResult(result);
    }
}
