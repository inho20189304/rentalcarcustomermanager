package com.airline.rentalcarmanager.repository;

import com.airline.rentalcarmanager.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
