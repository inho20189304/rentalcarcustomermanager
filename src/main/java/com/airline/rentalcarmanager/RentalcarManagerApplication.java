package com.airline.rentalcarmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RentalcarManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(RentalcarManagerApplication.class, args);
    }

}
