package com.airline.rentalcarmanager.controller;

import com.airline.rentalcarmanager.model.CommonResult;
import com.airline.rentalcarmanager.model.CustomerItem;
import com.airline.rentalcarmanager.model.CustomerRequest;
import com.airline.rentalcarmanager.model.ListResult;
import com.airline.rentalcarmanager.service.CustomerService;
import com.airline.rentalcarmanager.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {
    private final CustomerService customerService;
    @PostMapping("/data")
    public CommonResult setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(request);

        return ResponseService.getSuccessResult();
    }
    @GetMapping("/all")
    public ListResult<CustomerItem> getCustomers() {
        return ResponseService.getListResult(customerService.getCustomers(), true);
    }
}
